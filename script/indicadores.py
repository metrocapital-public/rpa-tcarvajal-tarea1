import json
import requests
from datetime import datetime
 

uri=GetVar("uri")
indicador=GetVar("indicador")
anio=GetVar("anio")

def get_indicador_x_anio(uri,indicador, anio):
    uri=f'{uri}/{indicador}/{anio}'
    print(uri)
    response = requests.get(uri)
    data = json.loads(response.text)
    return data

SetVar('data', get_indicador_x_anio(uri,indicador,anio))

datas=GetVar("data")
datas = datas.replace("'", "\"")
datas= json.loads(datas)

def get_nombre(datas):
    return datas['nombre']
SetVar('nombre_tipo',get_nombre(datas))

def get_unidad_medida(datas):
    return datas['unidad_medida']
SetVar('unidad_medida',get_unidad_medida(datas))
