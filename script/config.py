import os



from datetime import datetime 

from dotenv import dotenv_values

os.system('')

PATH_DIR = r'C:\Users\tomas\Documents\trabajoMetroCapital\Proyectos\tarea_1'

config = dotenv_values(fr'{PATH_DIR}\.env')
PATH_OUTPUT = os.path.join(PATH_DIR, 'output')
PATH_OUTPUT = PATH_OUTPUT.replace('\\', '/')
PATH_DIR = PATH_DIR.replace('\\', '/')

SetVar('uri', config['API_MINDICADOR_URI'])
SetVar('anio', config['ANIO_INICIO'])
SetVar('anio_final', datetime.now().strftime('%Y'))
SetVar('indicador', config['INDICADORES'])
SetVar('primera_carpeta',config['UBICACION_ARCHIVO'])


SetVar('mail',config['CORREO'])
SetVar('destinatario',config['DESTINATARIO'])
SetVar('password',config['CONTRASENIA'])



SetVar('path_output',PATH_OUTPUT)
SetVar('path_dir',PATH_DIR)

os.makedirs(PATH_OUTPUT, exist_ok=True)


