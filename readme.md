# Tarea 1 - Tomas Carvajal
```
  Este proyecto fue desarrolladon en RocketBot estudio, consta de dos robots uno el cual busca la informacion, crea los archivos, inserta la informacio y otro para que realice el envio al destinatario conjunto los archivos.
```
# PATHS
* para una correcta ejecucion, se recomienda modificar la ruta principal del proyecto script/config.py. en este dejar la ruta de tu carpeta raiz.
```
PATH_DIR = r'C:\Users\tomas\Documents\trabajoMetroCapital\Proyectos\tarea_1'
```
* ademas se debe cambiar dentro del RPA, en los siguientes bots, este debe quedar apuntando al config de tu proyecto creado en el punto anterior.
```
tarea_1 -> Comando Nº1
mail_tarea1 -> Comando Nº1
```

# Robots
```
  tarea_1
    -mail_tarea1
```
## Instalar dependencias
```
  se debe instalar tras usar pip install requieriments.txt
```
## Herramientas
```
  se utilizaron:
    Visual studio code
      -python
      -gitlab
    Postaman
      -Api: Mindicador
    RocketBot estudio
```
## Author
```
- tomas carvajal, tcarvajal@metrocapital.cl
```
